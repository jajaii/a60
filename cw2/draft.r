library(igraph)
load("~/a60/cw2/doctornet.Rdata")
par(oma = c(5,4,0,0) + 0.1, mar = c(0,0,1,1) + 0.1) # margin settings
plot(docnet2,vertex.size=0.3,edge.arrow.size=0.1, vertex.color="blue", vertex.frame.color=NA,xlim=c(-1,1),ylim=c(-1,1))

